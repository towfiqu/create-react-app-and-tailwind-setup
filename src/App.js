import React from 'react';
import './css/tailwind.css';

const App = () => {
  return (
    <div>
      <h2 className='text-4xl font-bold text-center text-blue-500'>
        Hello World
      </h2>
    </div>
  );
};

export default App;
